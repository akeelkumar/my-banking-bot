package org.apache.jsp.chat;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import dbclass.dbconne;

public final class chat_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<!--\r\n");
      out.write("To change this license header, choose License Headers in Project Properties.\r\n");
      out.write("To change this template file, choose Tools | Templates\r\n");
      out.write("and open the template in the editor.\r\n");
      out.write("-->\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <title>Bank BOt </title>\r\n");
      out.write("        <meta charset=\"UTF-8\">\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("        <link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />\t\r\n");
      out.write("        <script>\r\n");
      out.write("            var objDiv = document.getElementById(\"total\");\r\n");
      out.write("            objDiv.scrollTop = objDiv.scrollHeight;\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <div class=\"total\" style=\"width: 400px;height: 700px;border: 2px bold #0066ff;\">\r\n");
      out.write("      <div class=\"menu\">\r\n");
      out.write("          <div class=\"back\"><i class=\"fa fa-chevron-left\"><a href=\"back.jsp\"> <img src=\"http://i.imgur.com/DY6gND0.png\" draggable=\"false\"/></a></i></div>\r\n");
      out.write("            <div class=\"name\">Welcome to Banking Chat</div>\r\n");
      out.write("            <div id=\"demo\" class=\"last\"></div>\r\n");
      out.write("            <script>\r\n");
      out.write("                var d = new Date();\r\n");
      out.write("                document.getElementById(\"demo\").innerHTML = d.toDateString();\r\n");
      out.write("            </script>\r\n");
      out.write("        </div>\r\n");
      out.write("    <ol class=\"chat\" style=\"transform-origin: 50% 50%;\" onscroll=\"reverseScroll()\">\r\n");
      out.write("       \r\n");
      out.write("        ");

	String mail = session.getAttribute("email").toString();
        String s="select * from message";
        dbconne db1=new dbconne();
        db1.select(s);
        while(db1.rs.next())
        {
            String name=db1.rs.getString("mail");
            if(name.contains(mail))
            {
        
      out.write("\r\n");
      out.write("    \r\n");
      out.write("    <li class=\"self\">\r\n");
      out.write("        <div class=\"avatar\"></div>\r\n");
      out.write("      <div class=\"msg\">\r\n");
      out.write("       \r\n");
      out.write("       \r\n");
      out.write("        <p>");
      out.print(db1.rs.getString("message"));
      out.write("</p>\r\n");
      out.write("       \r\n");
      out.write("      </div>\r\n");
      out.write("    </li>\r\n");
      out.write("    ");

            }
            else
            {
    
      out.write("\r\n");
      out.write("    <li class=\"other\">\r\n");
      out.write("        <div class=\"avatar\"></div>\r\n");
      out.write("      <div class=\"msg\">\r\n");
      out.write("       \r\n");
      out.write("        <p>");
      out.print(db1.rs.getString("message"));
      out.write("</p>\r\n");
      out.write("        \r\n");
      out.write("      </div>\r\n");
      out.write("    </li>\r\n");
      out.write("    ");

            }
        }
            
      out.write("\r\n");
      out.write("    </ol>\r\n");
      out.write("            <form action=\"#\">\r\n");
      out.write("                <input type=\"submit\" class=\"textarea\" name=\"post\" value=\"POST\">\r\n");
      out.write("                <input class=\"textarea\" type=\"text\" name=\"msg\" placeholder=\"Type here!\">\r\n");
      out.write("    \r\n");
      out.write("                <div class=\"emojis\"></div>\r\n");
      out.write("            </form>\r\n");
      out.write("        </div>\r\n");
      out.write("       \r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");

    if(request.getParameter("post")!=null)
    {
        String msg=request.getParameter("msg");
        dbconne db=new dbconne();
        String str="Insert into message values (0,'"+mail+"','"+msg+"')";
        db.insert(str);
        response.sendRedirect("chat.jsp");
    
    }
    
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
