package org.apache.jsp.chat;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import dbclass.dbconne;

public final class chat1_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!--\n");
      out.write("To change this license header, choose License Headers in Project Properties.\n");
      out.write("To change this template file, choose Tools | Templates\n");
      out.write("and open the template in the editor.\n");
      out.write("-->\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Banking Bot</title>\n");
      out.write("        <meta charset=\"UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />\t\n");
      out.write("        <script>\n");
      out.write("            var objDiv = document.getElementById(\"total\");\n");
      out.write("            objDiv.scrollTop = objDiv.scrollHeight;\n");
      out.write("        </script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"total\" style=\"width: 400px;height: 700px;border: 2px bold #0066ff;\">\n");
      out.write("      <div class=\"menu\">\n");
      out.write("          <div class=\"back\"><i class=\"fa fa-chevron-left\"><a href=\"back.jsp\"> <img src=\"http://i.imgur.com/DY6gND0.png\" draggable=\"false\"/></a></i></div>\n");
      out.write("            <div class=\"name\">Welcome to Admin ChatBox</div>\n");
      out.write("            <div id=\"demo\" class=\"last\"></div>\n");
      out.write("            <script>\n");
      out.write("                var d = new Date();\n");
      out.write("                document.getElementById(\"demo\").innerHTML = d.toDateString();\n");
      out.write("            </script>\n");
      out.write("        </div>\n");
      out.write("    <ol class=\"chat\" style=\"transform-origin: 50% 50%;\" onscroll=\"reverseScroll()\">\n");
      out.write("       \n");
      out.write("        ");

	String mail = session.getAttribute("email").toString();
        String s="select * from messagewhere status = 1 ";
        dbconne db1=new dbconne();
        db1.select(s);
        while(db1.rs.next())
        {
            String name=db1.rs.getString("mail");
            if(name.contains("admin@gmail.com"))
            {
        
      out.write("\n");
      out.write("    \n");
      out.write("    <li class=\"self\">\n");
      out.write("        <div class=\"avatar\"></div>\n");
      out.write("      <div class=\"msg\">\n");
      out.write("       \n");
      out.write("       \n");
      out.write("        <p>");
      out.print(db1.rs.getString("message"));
      out.write("</p>\n");
      out.write("       \n");
      out.write("      </div>\n");
      out.write("    </li>\n");
      out.write("    ");

            }
            else
            {
    
      out.write("\n");
      out.write("    <li class=\"other\">\n");
      out.write("        <div class=\"avatar\"></div>\n");
      out.write("      <div class=\"msg\">\n");
      out.write("       \n");
      out.write("        <p>");
      out.print(db1.rs.getString("message"));
      out.write("</p>\n");
      out.write("        \n");
      out.write("      </div>\n");
      out.write("    </li>\n");
      out.write("    ");

            }
        }
            
      out.write("\n");
      out.write("    </ol>\n");
      out.write("            <form action=\"#\">\n");
      out.write("                <input type=\"submit\" class=\"textarea\" name=\"post\" value=\"POST\">\n");
      out.write("                <input class=\"textarea\" type=\"text\" name=\"msg\" placeholder=\"Type here!\">\n");
      out.write("                    <input type =\"submit\" value=\"Clear\" name=\"Clear\">\n");
      out.write("                <div class=\"emojis\"></div>\n");
      out.write("            \n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("       \n");
      out.write("    </body>\n");
      out.write("</html>\n");

    if(request.getParameter("post")!=null)
    {
        String msg=request.getParameter("msg");
        dbconne db=new dbconne();
        String str="Insert into message values ('admin@gmail.com','"+msg+"',1)";
        db.insert(str);
        response.sendRedirect("chat1.jsp");
    
    }
        
if(request.getParameter("Clear")!=null)
{
        dbconne db3 = new dbconne();
        String str1 = "update message set status = 0 ";
        db3.insert(str1);
        response.sendRedirect("chat1.jsp");
}
    
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
