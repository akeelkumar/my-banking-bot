package org.apache.jsp.branch_0020locator;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class map_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("  <head>\n");
      out.write("    <title>Place searches</title>\n");
      out.write("    <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\">\n");
      out.write("    <meta charset=\"utf-8\">\n");
      out.write("    <style>\n");
      out.write("      /* Always set the map height explicitly to define the size of the div\n");
      out.write("       * element that contains the map. */\n");
      out.write("      #map {\n");
      out.write("        height: 100%;\n");
      out.write("      }\n");
      out.write("      /* Optional: Makes the sample page fill the window. */\n");
      out.write("      html, body {\n");
      out.write("        height: 100%;\n");
      out.write("        margin: 0;\n");
      out.write("        padding: 0;\n");
      out.write("      }\n");
      out.write("    </style>\n");
      out.write("    <script>\n");
      out.write("      // This example requires the Places library. Include the libraries=places\n");
      out.write("      // parameter when you first load the API. For example:\n");
      out.write("      // <script src=\"https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places\">\n");
      out.write("\n");
      out.write("      var map;\n");
      out.write("      var infowindow;\n");
      out.write("\n");
      out.write("      function initMap() {\n");
      out.write("        var pyrmont = {lat: -33.867, lng: 151.195};\n");
      out.write("\n");
      out.write("        map = new google.maps.Map(document.getElementById('map'), {\n");
      out.write("          center: pyrmont,\n");
      out.write("          zoom: 15\n");
      out.write("        });\n");
      out.write("\n");
      out.write("        infowindow = new google.maps.InfoWindow();\n");
      out.write("        var service = new google.maps.places.PlacesService(map);\n");
      out.write("        service.nearbySearch({\n");
      out.write("          location: pyrmont,\n");
      out.write("          radius: 500,\n");
      out.write("          type: ['store']\n");
      out.write("        }, callback);\n");
      out.write("      }\n");
      out.write("\n");
      out.write("      function callback(results, status) {\n");
      out.write("        if (status === google.maps.places.PlacesServiceStatus.OK) {\n");
      out.write("          for (var i = 0; i < results.length; i++) {\n");
      out.write("            createMarker(results[i]);\n");
      out.write("          }\n");
      out.write("        }\n");
      out.write("      }\n");
      out.write("\n");
      out.write("      function createMarker(place) {\n");
      out.write("        var placeLoc = place.geometry.location;\n");
      out.write("        var marker = new google.maps.Marker({\n");
      out.write("          map: map,\n");
      out.write("          position: place.geometry.location\n");
      out.write("        });\n");
      out.write("\n");
      out.write("        google.maps.event.addListener(marker, 'click', function() {\n");
      out.write("          infowindow.setContent(place.name);\n");
      out.write("          infowindow.open(map, this);\n");
      out.write("        });\n");
      out.write("      }\n");
      out.write("    </script>\n");
      out.write("  </head>\n");
      out.write("  <body>\n");
      out.write("    <div id=\"map\"></div>\n");
      out.write("    <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBVXwgSfvHXXP-VibYL4XgyeMzagyJnEdk&libraries=places&callback=initMap\" async defer></script>\n");
      out.write("  </body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
