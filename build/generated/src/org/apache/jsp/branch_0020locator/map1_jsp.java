package org.apache.jsp.branch_0020locator;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import java.sql.Statement;
import dbclass.dbconne;

public final class map1_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("<title>WELCOME | HOME</title>\n");
      out.write("<link type=\"text/css\" rel=\"stylesheet\" href=\"css/styles.css\" />\n");
      out.write("<div id=\"header\"><font color=\"#FFFFFF\" size=\"32\"><center>BEST KEYWORD COVER SEARCH</center></font></div>\n");
      out.write("<style>\n");
      out.write("      \n");
      out.write("      #map {\n");
      out.write("        height: 95%;\n");
      out.write("\t\tbackground-color:#666666;\n");
      out.write("      }\n");
      out.write("    </style>\n");
      out.write("<script>\n");
      out.write("/*\n");
      out.write("function getLocation() {\n");
      out.write("    if (navigator.geolocation) {\n");
      out.write("        navigator.geolocation.getCurrentPosition(showPosition);\n");
      out.write("    } else { \n");
      out.write("        x.innerHTML = \"Geolocation is not supported by this browser.\";\n");
      out.write("    }\n");
      out.write("}\n");
      out.write("\n");
      out.write("function showPosition(position) {\n");
      out.write("    var x=position.coords.latitude;\t\n");
      out.write("    document.getElementById(\"lat\").value = x;\n");
      out.write("    var x=position.coords.longitude;\t\n");
      out.write("    document.getElementById(\"log\").value = x;\n");
      out.write("}*/\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("\t    <script>\n");
      out.write("// This example requires the Places library. Include the libraries=places\n");
      out.write("// parameter when you first load the API. For example:\n");
      out.write("// <script src=\"https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places\">\n");
      out.write("\n");
      out.write("var map;\n");
      out.write("var infowindow;\n");
      out.write("\n");
      out.write("function initMap() {\n");
      out.write("  var latlngStr = document.getElementById('lat').value;\n");
      out.write("  var latlngStr1 = document.getElementById('log').value;\n");
      out.write("  var pyrmont = {lat: parseFloat(latlngStr), lng: parseFloat(latlngStr1)};\n");
      out.write("\n");
      out.write("\n");
      out.write(" \n");
      out.write("  map = new google.maps.Map(document.getElementById('map'), {\n");
      out.write("    center: pyrmont,\n");
      out.write("    zoom: 15\n");
      out.write("  });\n");
      out.write("\n");
      out.write("\n");
      out.write("  infowindow = new google.maps.InfoWindow();\n");
      out.write("\n");
      out.write("  var service = new google.maps.places.PlacesService(map);\n");
      out.write("  service.nearbySearch({\n");
      out.write("    location: pyrmont,\n");
      out.write("    radius: 1000,\n");
      out.write("    types: ['");
      out.print(request.getParameter("search"));
      out.write("']\n");
      out.write("  }, callback);\n");
      out.write("}\n");
      out.write("\n");
      out.write("function callback(results, status) {\n");
      out.write("  if (status === google.maps.places.PlacesServiceStatus.OK) {\n");
      out.write("    for (var i = 0; i < results.length; i++) {\n");
      out.write("      createMarker(results[i]);\n");
      out.write("\t // alert(results[i].name);\n");
      out.write("    }\n");
      out.write("  }\n");
      out.write("}\n");
      out.write("\n");
      out.write("function createMarker(place) {\n");
      out.write("  var placeLoc = place.geometry.location;\n");
      out.write("  var marker = new google.maps.Marker({\n");
      out.write("    map: map,\n");
      out.write("    position: place.geometry.location\n");
      out.write("  });\n");
      out.write("\n");
      out.write("  google.maps.event.addListener(marker, 'click', function() {\n");
      out.write("      alert(place.name+\"        \"+place.geometry.location.lat()+\"   \"+place.geometry.location.lng());\n");
      out.write("    infowindow.setContent(place.name);\n");
      out.write("    infowindow.open(map, this);\n");
      out.write("  });\n");
      out.write("}\n");
      out.write("\n");
      out.write("    </script>\n");
      out.write("    \n");
      out.write(" \n");
      out.write("\n");
      out.write("\t\n");
      out.write("</head>\n");
      out.write("<body onLoad=\"getLocation()\">\n");
      out.write("\t\t<div id=\"tabs\">\n");
      out.write("            \n");
      out.write("        </div> \n");
      out.write("        <br /><br /><br /><br />\n");
      out.write("        <div id=\"container\">\n");
      out.write("        \t<div class=\"contents\">\n");
      out.write("            \t<center>\n");
      out.write("                    <form method=\"post\">\n");
      out.write("                      <table>\n");
      out.write("                    \t<tr>\n");
      out.write("                        \t<td>Search Query </td>\n");
      out.write("                            <td> : </td>\n");
      out.write("                            <td><input name=\"search\" type=\"text\" id=\"search\" required=\"true\" list=\"keyword\" />\n");
      out.write("                              \n");
      out.write("                               ");

                                try
                                {   
                                      String a;
                                      String s="select * from statelist where city_name = 'place.value'";
                                      dbconne db= new dbconne();
                                      db.select(s);
                                   /* Statement st=db.con.createStatement();
                                    String email=session.getAttribute("user").toString();
                                    ResultSet rs=st.executeQuery("select * from current_location where email='"+email+"' order by id desc");*/
                                    if(db.rs.next())
                                    {
                                        
      out.write("\n");
      out.write("                                        <input type=\"hidden\" name=\"lat\" id=\"lat\" value=\"");
      out.print(db.rs.getString(3));
      out.write("\"/>\n");
      out.write("                                        <input type=\"hidden\" name=\"log\" id=\"log\" value=\"");
      out.print(db.rs.getString(4));
      out.write("\"/>\n");
      out.write("                                        Place :<input type=\"text\" name=\"place\" id=\"places\" value=\"");
      out.print(db.rs.getString(2));
      out.write("\"/>\n");
      out.write("                                        ");

                                    }
                                    else
                                    {
                                        
      out.write("\n");
      out.write("                                        Place :<input type=\"text\" name=\"place\" id=\"place\"/>\n");
      out.write("                                        ");

                                    }
                                            
                                }
                                catch(Exception e)
                                {
                                    System.out.println("error  "+e);
                                }
                                
                                
      out.write("\n");
      out.write("                                <datalist id=\"keyword\">\n");
      out.write("                                    <option value=\"accounting\">\n");
      out.write("<option value=\"airport\">\n");
      out.write("<option value=\"amusement_park\">\n");
      out.write("<option value=\"aquarium\">\n");
      out.write("<option value=\"art_gallery\">\n");
      out.write("<option value=\"atm\">\n");
      out.write("<option value=\"bakery\">\n");
      out.write("<option value=\"bank\">\n");
      out.write("<option value=\"bar\">\n");
      out.write("<option value=\"beauty_salon\">\n");
      out.write("<option value=\"bicycle_store\">\n");
      out.write("<option value=\"book_store\">\n");
      out.write("<option value=\"bowling_alley\">\n");
      out.write("<option value=\"bus_station\">\n");
      out.write("<option value=\"cafe\">\n");
      out.write("<option value=\"campground\">\n");
      out.write("<option value=\"car_dealer\">\n");
      out.write("<option value=\"car_rental\">\n");
      out.write("<option value=\"car_repair\">\n");
      out.write("<option value=\"car_wash\">\n");
      out.write("<option value=\"casino\">\n");
      out.write("<option value=\"cemetery\">\n");
      out.write("<option value=\"church\">\n");
      out.write("<option value=\"city_hall\">\n");
      out.write("<option value=\"clothing_store\">\n");
      out.write("<option value=\"convenience_store\">\n");
      out.write("<option value=\"courthouse\">\n");
      out.write("<option value=\"dentist\">\n");
      out.write("<option value=\"department_store\">\n");
      out.write("<option value=\"doctor\">\n");
      out.write("<option value=\"electrician\">\n");
      out.write("<option value=\"electronics_store\">\n");
      out.write("<option value=\"embassy\">\n");
      out.write("<option value=\"establishment\">\n");
      out.write("<option value=\"finance\">\n");
      out.write("<option value=\"fire_station\">\n");
      out.write("<option value=\"florist\">\n");
      out.write("<option value=\"food\">\n");
      out.write("<option value=\"funeral_home\">\n");
      out.write("<option value=\"furniture_store\">\n");
      out.write("<option value=\"gas_station\">\n");
      out.write("<option value=\"general_contractor\">\n");
      out.write("<option value=\"grocery_or_supermarket\">\n");
      out.write("<option value=\"gym\">\n");
      out.write("<option value=\"hair_care\">\n");
      out.write("<option value=\"hardware_store\">\n");
      out.write("<option value=\"health\">\n");
      out.write("<option value=\"hindu_temple\">\n");
      out.write("<option value=\"home_goods_store\">\n");
      out.write("<option value=\"hospital\">\n");
      out.write("<option value=\"insurance_agency\">\n");
      out.write("<option value=\"jewelry_store\">\n");
      out.write("<option value=\"laundry\">\n");
      out.write("<option value=\"lawyer\">\n");
      out.write("<option value=\"library\">\n");
      out.write("<option value=\"liquor_store\">\n");
      out.write("<option value=\"local_government_office\">\n");
      out.write("<option value=\"locksmith\">\n");
      out.write("<option value=\"lodging\">\n");
      out.write("<option value=\"meal_delivery\">\n");
      out.write("<option value=\"meal_takeaway\">\n");
      out.write("<option value=\"mosque\">\n");
      out.write("<option value=\"movie_rental\">\n");
      out.write("<option value=\"movie_theater\">\n");
      out.write("<option value=\"moving_company\">\n");
      out.write("<option value=\"museum\">\n");
      out.write("<option value=\"night_club\">\n");
      out.write("<option value=\"painter\">\n");
      out.write("<option value=\"park\">\n");
      out.write("<option value=\"parking\">\n");
      out.write("<option value=\"pet_store\">\n");
      out.write("<option value=\"pharmacy\">\n");
      out.write("<option value=\"physiotherapist\">\n");
      out.write("<option value=\"place_of_worship\">\n");
      out.write("<option value=\"plumber\">\n");
      out.write("<option value=\"police\">\n");
      out.write("<option value=\"post_office\">\n");
      out.write("<option value=\"real_estate_agency\">\n");
      out.write("<option value=\"restaurant\">\n");
      out.write("<option value=\"roofing_contractor\">\n");
      out.write("<option value=\"rv_park\">\n");
      out.write("<option value=\"school\">\n");
      out.write("<option value=\"shoe_store\">\n");
      out.write("<option value=\"shopping_mall\">\n");
      out.write("<option value=\"spa\">\n");
      out.write("<option value=\"stadium\">\n");
      out.write("<option value=\"storage\">\n");
      out.write("<option value=\"store\">\n");
      out.write("<option value=\"subway_station\">\n");
      out.write("<option value=\"synagogue\">\n");
      out.write("<option value=\"taxi_stand\">\n");
      out.write("<option value=\"train_station\">\n");
      out.write("<option value=\"ravel_agency\">\n");
      out.write("<option value=\"university\">\n");
      out.write("<option value=\"veterinary_care\">\n");
      out.write("<option value=\"zoo\">\n");
      out.write("                     </datalist>\n");
      out.write("                            </td>\n");
      out.write("                        </tr>\n");
      out.write("                        <tr>\n");
      out.write("                        \t<td></td>\n");
      out.write("                            <td></td>\n");
      out.write("                            <td><input type=\"submit\" name=\"Submit\" value=\"Submit\" /></td>\n");
      out.write("                        </tr>\n");
      out.write("                    </table>\n");
      out.write("                        \n");
      out.write("                        ");

                        if(request.getParameter("Submit")!=null)
                        {
                            
      out.write("\n");
      out.write("                             <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBVXwgSfvHXXP-VibYL4XgyeMzagyJnEdk&signed_in=true&libraries=places&callback=initMap\" async defer></script>\n");
      out.write("                            ");

                        }
                        else
                        {
                            
      out.write("\n");
      out.write("                            <script>\n");
      out.write("                                alert(\"No Data to Display\");\n");
      out.write("                            </script>\n");
      out.write("                            ");

                        }
                        
                        
      out.write("\n");
      out.write("                    </form>\n");
      out.write("                    <div id=\"map\">\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                 \n");
      out.write("            \t</center>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
