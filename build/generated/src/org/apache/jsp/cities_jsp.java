package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import dbclass.dbconne;

public final class cities_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");

   String city=request.getParameter("city");
    String s="select * from statelist where city_id='"+city+"'";
    dbconne db= new dbconne();
    db.select(s);
    if(db.rs.next())
    {
        String lat=db.rs.getString("latitude");
        String lon=db.rs.getString("longitude");
        String s1=lat.substring(0,5);
        String s2=lon.substring(0,5);
        //out.println(s1+" and "+s2);
   

      out.write("\n");
      out.write("<html class=\"no-js\">\n");
      out.write("    <head>\n");
      out.write("        <!-- Basic Page Needs\n");
      out.write("        ================================================== -->\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n");
      out.write("        <link rel=\"icon\" type=\"image/png\" href=\"images/favicon.png\">\n");
      out.write("        <title>Banking Bot</title>\n");
      out.write("        <meta name=\"description\" content=\"\">\n");
      out.write("        <meta name=\"keywords\" content=\"\">\n");
      out.write("        <meta name=\"author\" content=\"\">\n");
      out.write("        <!-- Mobile Specific Metas\n");
      out.write("        ================================================== -->\n");
      out.write("        <meta name=\"format-detection\" content=\"telephone=no\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        \n");
      out.write("        <!-- Template CSS Files\n");
      out.write("        ================================================== -->\n");
      out.write("        <!-- Twitter Bootstrs CSS -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("        <!-- Ionicons Fonts Css -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/ionicons.min.css\">\n");
      out.write("        <!-- animate css -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/animate.css\">\n");
      out.write("        <!-- Hero area slider css-->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/slider.css\">\n");
      out.write("        <!-- owl craousel css -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/owl.carousel.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/owl.theme.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/jquery.fancybox.css\">\n");
      out.write("        <!-- template main css file -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/main.css\">\n");
      out.write("        <!-- responsive css -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/responsive.css\">\n");
      out.write("        \n");
      out.write("        <!-- Template Javascript Files\n");
      out.write("        ================================================== -->\n");
      out.write("        <!-- modernizr js -->\n");
      out.write("        <script src=\"js/vendor/modernizr-2.6.2.min.js\"></script>\n");
      out.write("        <!-- jquery -->\n");
      out.write("        <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>\n");
      out.write("        <!-- owl carouserl js -->\n");
      out.write("        <script src=\"js/owl.carousel.min.js\"></script>\n");
      out.write("        <!-- bootstrap js -->\n");
      out.write("\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("        <!-- wow js -->\n");
      out.write("        <script src=\"js/wow.min.js\"></script>\n");
      out.write("        <!-- slider js -->\n");
      out.write("        <script src=\"js/slider.js\"></script>\n");
      out.write("        <script src=\"js/jquery.fancybox.js\"></script>\n");
      out.write("        <!-- template main js -->\n");
      out.write("        <script src=\"js/main.js\"></script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <!--\n");
      out.write("        ==================================================\n");
      out.write("        Header Section Start\n");
      out.write("        ================================================== -->\n");
      out.write("        <header id=\"top-bar\" class=\"navbar-fixed-top animated-header\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"navbar-header\">\n");
      out.write("                    <!-- responsive nav button -->\n");
      out.write("                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">\n");
      out.write("                    <span class=\"sr-only\">Toggle navigation</span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    </button>\n");
      out.write("                    <!-- /responsive nav button -->\n");
      out.write("                    \n");
      out.write("                    <!-- logo -->\n");
      out.write("                    <div class=\"navbar-brand\">\n");
      out.write("                        <a href=\"index.html\" >\n");
      out.write("                            <img src=\"images/bba.png\" alt=\"\">\n");
      out.write("                        </a>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- /logo -->\n");
      out.write("                </div>\n");
      out.write("                <!-- main menu -->\n");
      out.write("                        <nav class=\"collapse navbar-collapse navbar-right\" role=\"navigation\">\n");
      out.write("                    <div class=\"main-menu\">\n");
      out.write("                        <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"index.html\" >Home</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li><a href=\"about.html\">About</a></li>\n");
      out.write("                            <li><a href=\"talks.jsp\">Service</a></li>\n");
      out.write("                            \n");
      out.write("                            <li><a href=\"feedback.jsp\">FeedBack</a></li>\n");
      out.write("                            <li><a href=\"newsfeed.jsp\">LatestNews</a></li>\n");
      out.write("                           \n");
      out.write("                            <li><a href =\"searchcity.jsp\">Bank Locator </a><li>\n");
      out.write("                            \n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </nav>\n");
      out.write("                <!-- /main nav -->\n");
      out.write("            </div>\n");
      out.write("        </header>\n");
      out.write("        \n");
      out.write("    \n");
      out.write("\n");
      out.write("<style type=\"text/css\">\n");
      out.write("div#map_container{\n");
      out.write("\twidth:100%;\n");
      out.write("\theight:800px;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("<script type=\"text/javascript\"\n");
      out.write("   src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyAJ4apvmQxF7lCwv7SqQGQlq4nsRGTc8nI&sensor=false\"></script>\n");
      out.write("\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("  function loadMap(latitude,longitude) {\n");
      out.write("    var latlng = new google.maps.LatLng(latitude,longitude);\n");
      out.write("    var myOptions = {\n");
      out.write("      zoom: 6,\n");
      out.write("      center: latlng,\n");
      out.write("      mapTypeId: google.maps.MapTypeId.ROADMAP\n");
      out.write("    };\n");
      out.write("    var map = new google.maps.Map(document.getElementById(\"map_container\"),myOptions);\n");
      out.write("\n");
      out.write("    var marker = new google.maps.Marker({\n");
      out.write("      position: latlng,\n");
      out.write("      map: map,\n");
      out.write("      title:\"my hometown, Malim Nawar!\"\n");
      out.write("    });\n");
      out.write("\n");
      out.write("  }\n");
      out.write("</script>\n");
      out.write("</head>\n");
      out.write("   <body onload=\"loadMap(");
      out.print(s1);
      out.write(',');
      out.print(s2);
      out.write(")\">\n");
      out.write("<div id=\"map_container\"></div>\n");
      out.write("\n");
      out.write("<footer id=\"footer\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"col-md-8\">\n");
      out.write("                        <p class=\"copyright\">Copyright: <span>2015</span> . Design and Developed by <a href=\"http://www.Themefisher.com\">Themefisher</a></p>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-md-4\">\n");
      out.write("                        <!-- Social Media -->\n");
      out.write("                        <ul class=\"social\">\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"http://wwww.fb.com/themefisher\" class=\"Facebook\">\n");
      out.write("                                    <i class=\"ion-social-facebook\"></i>\n");
      out.write("                                </a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"http://wwww.twitter.com/themefisher\" class=\"Twitter\">\n");
      out.write("                                    <i class=\"ion-social-twitter\"></i>\n");
      out.write("                                </a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"#\" class=\"Linkedin\">\n");
      out.write("                                    <i class=\"ion-social-linkedin\"></i>\n");
      out.write("                                </a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"http://wwww.fb.com/themefisher\" class=\"Google Plus\">\n");
      out.write("                                    <i class=\"ion-social-googleplus\"></i>\n");
      out.write("                                </a>\n");
      out.write("                            </li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </footer> <!-- /#footer -->\n");
      out.write("                \n");
      out.write("        </body>\n");
      out.write("    </html>\n");

     }
    
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
