package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class newsfeed_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\n");
      out.write("    <head\n");
      out.write("        \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("      \n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n");
      out.write("        <link rel=\"icon\" type=\"image/png\" href=\"images/favicon.png\">\n");
      out.write("        <title>Banking bot</title>\n");
      out.write("        <meta name=\"description\" content=\"\">\n");
      out.write("        <meta name=\"keywords\" content=\"\">\n");
      out.write("        <meta name=\"author\" content=\"\">\n");
      out.write("        <!-- Mobile Specific Metas\n");
      out.write("        ================================================== -->\n");
      out.write("        <meta name=\"format-detection\" content=\"telephone=no\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        \n");
      out.write("        <!-- Template CSS Files\n");
      out.write("        ================================================== -->\n");
      out.write("        <!-- Twitter Bootstrs CSS -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("        <!-- Ionicons Fonts Css -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/ionicons.min.css\">\n");
      out.write("        <!-- animate css -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/animate.css\">\n");
      out.write("        <!-- Hero area slider css-->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/slider.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/table.css\">\n");
      out.write("        <!-- owl craousel css -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/owl.carousel.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/owl.theme.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/jquery.fancybox.css\">\n");
      out.write("        <!-- template main css file -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/main.css\">\n");
      out.write("        <!-- responsive css -->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/responsive.css\">\n");
      out.write("        \n");
      out.write("        <!-- Template Javascript Files\n");
      out.write("        ================================================== -->\n");
      out.write("        <!-- modernizr js -->\n");
      out.write("        <script src=\"js/vendor/modernizr-2.6.2.min.js\"></script>\n");
      out.write("        <!-- jquery -->\n");
      out.write("        <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>\n");
      out.write("        <!-- owl carouserl js -->\n");
      out.write("        <script src=\"js/owl.carousel.min.js\"></script>\n");
      out.write("        <!-- bootstrap js -->\n");
      out.write("\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("        <!-- wow js -->\n");
      out.write("        <script src=\"js/wow.min.js\"></script>\n");
      out.write("        <!-- slider js -->\n");
      out.write("        <script src=\"js/slider.js\"></script>\n");
      out.write("        <script src=\"js/jquery.fancybox.js\"></script>\n");
      out.write("        <!-- template main js -->\n");
      out.write("        <script src=\"js/main.js\"></script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <!--\n");
      out.write("        ==================================================\n");
      out.write("        Header Section Start\n");
      out.write("        ================================================== -->\n");
      out.write("        <header id=\"top-bar\" class=\"navbar-fixed-top animated-header\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"navbar-header\">\n");
      out.write("                    <!-- responsive nav button -->\n");
      out.write("                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">\n");
      out.write("                    <span class=\"sr-only\">Toggle navigation</span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    <span class=\"icon-bar\"></span>\n");
      out.write("                    </button>\n");
      out.write("                    <!-- /responsive nav button -->\n");
      out.write("                    \n");
      out.write("                    <!-- logo -->\n");
      out.write("                    <div class=\"navbar-brand\">\n");
      out.write("                        <a href=\"index.html\" >\n");
      out.write("                            <img src=\"images/bba.png\" alt=\"\">\n");
      out.write("                        </a>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- /logo -->\n");
      out.write("                </div>\n");
      out.write("                <!-- main menu -->\n");
      out.write("                <nav class=\"collapse navbar-collapse navbar-right\" role=\"navigation\">\n");
      out.write("                    <div class=\"main-menu\">\n");
      out.write("                        <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"index.html\" >Home</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li><a href=\"about.html\">About</a></li>\n");
      out.write("                            <li><a href=\"service.html\">Service</a></li>\n");
      out.write("                            <li class=\"dropdown\">\n");
      out.write("                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Pages <span class=\"caret\"></span></a>\n");
      out.write("                                <div class=\"dropdown-menu\">\n");
      out.write("                                    <ul>\n");
      out.write("                                        <li><a href=\"404.html\">404 Page</a></li>\n");
      out.write("                                        <li><a href=\"gallery.html\">Gallery</a></li>\n");
      out.write("                                    </ul>\n");
      out.write("                                </div>\n");
      out.write("                            </li>\n");
      out.write("                            <li class=\"dropdown\">\n");
      out.write("                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Blog <span class=\"caret\"></span></a>\n");
      out.write("                                <div class=\"dropdown-menu\">\n");
      out.write("                                    <ul>\n");
      out.write("                                        <li><a href=\"blog-fullwidth.html\">Blog Full</a></li>\n");
      out.write("                                        <li><a href=\"blog-left-sidebar.html\">Blog Left sidebar</a></li>\n");
      out.write("                                        <li><a href=\"blog-right-sidebar.html\">Blog Right sidebar</a></li>\n");
      out.write("                                    </ul>\n");
      out.write("                                </div>\n");
      out.write("                            </li>\n");
      out.write("                            <li><a href=\"contact.html\">Contact</a></li>\n");
      out.write("                            <li><a href =\"searchcity.jsp\">Bank Locator </a><li>\n");
      out.write("                            \n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </nav>\n");
      out.write("                <!-- /main nav -->\n");
      out.write("            </div>\n");
      out.write("        </header>\n");
      out.write("        <br><br><br><br><br>\n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        ");

        response.sendRedirect("http://localhost/rss/index.php");
        
      out.write("\n");
      out.write("                \n");
      out.write("                   \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        <footer id=\"footer\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div class=\"col-md-8\">\n");
      out.write("                    <p class=\"copyright\">Copyright: <span>2015</span> . Design and Developed by <a href=\"http://www.Themefisher.com\">Themefisher</a></p>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"col-md-4\">\n");
      out.write("                    <!-- Social Media -->\n");
      out.write("                    <ul class=\"social\">\n");
      out.write("                        <li>\n");
      out.write("                            <a href=\"#\" class=\"Facebook\">\n");
      out.write("                                <i class=\"ion-social-facebook\"></i>\n");
      out.write("                            </a>\n");
      out.write("                        </li>\n");
      out.write("                        <li>\n");
      out.write("                            <a href=\"#\" class=\"Twitter\">\n");
      out.write("                                <i class=\"ion-social-twitter\"></i>\n");
      out.write("                            </a>\n");
      out.write("                        </li>\n");
      out.write("                        <li>\n");
      out.write("                            <a href=\"#\" class=\"Linkedin\">\n");
      out.write("                                <i class=\"ion-social-linkedin\"></i>\n");
      out.write("                            </a>\n");
      out.write("                        </li>\n");
      out.write("                        <li>\n");
      out.write("                            <a href=\"#\" class=\"Google Plus\">\n");
      out.write("                                <i class=\"ion-social-googleplus\"></i>\n");
      out.write("                            </a>\n");
      out.write("                        </li>\n");
      out.write("                        \n");
      out.write("                    </ul>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </footer> <!-- /#footer ---->\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
