<%-- 
    Document   : talkiee
    Created on : 28 Mar, 2017, 11:47:52 AM
    Author     : akhil
--%>
<%@page import="dbclass.dbconne"%>

<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="algpack.MainTestTextClassifier"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.File"%>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Banking Bot</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="css/ionicons.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="css/slider.css">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <!-- template main css file -->
        <link rel="stylesheet" href="css/main.css">
        <!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
        
        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- owl carouserl js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- bootstrap js -->

        <script src="js/bootstrap.min.js"></script>
        <!-- wow js -->
        <script src="js/wow.min.js"></script>
        <!-- slider js -->
        <script src="js/slider.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <!-- template main js -->
        <script src="js/main.js"></script>
    </head>
    <body>
        <!--
        ==================================================
        Header Section Start
        ================================================== -->
        <header id="top-bar" class="navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->
                    
                    <!-- logo -->
                    <div class="navbar-brand">
                        <a href="index.html" >
                            <img src="images/bba.png" alt="">
                        </a>
                    </div>
                    <!-- /logo -->
                </div>
                <!-- main menu -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="index.html" >Home</a>
                            </li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="talk.jsp">Service</a></li>
                           
                        </ul>
                    </div>
                </nav>
                <!-- /main nav -->
            </div>
            
        </header>

        
<div class="container">
    <div class="col-md-8">
       <form action ="#" method ="post">
      
<select name="bank">
    <h2>Select your Bank</h2>
  <option value="AXIS">AXIS</option>
  <option value="ICICI">ICICI</option>
  <option value="HDFC">HDFC</option>
  <option value="SBI">SBI</option>
    <option value="SBT">SBT</option>
</select>
<button type="submit" class="btn btn-default" name = "submit">Submit</button>
    </form>
    </div>                     
</div>
        
        
<%
    try
{
    dbconne db = new dbconne();
 if(request.getParameter("submit")!=null)
        {
    if(request.getParameter("bank").contains("AXIS")){
            String sql= "select * from tbl_axis";
            db.select(sql);

            }
            else if(request.getParameter("bank").contains("ICICI"))
            {
               String sql1= "select * from tbl_icici";
            db.select(sql1); 
            }
             else if(request.getParameter("bank").contains("HDFC"))
            {
               String sql2= "select * from tbl_hdfc";
            db.select(sql2); 
            }
             else if(request.getParameter("bank").contains("SBI"))
            {
               String sql3= "select * from tbl_sbi";
            db.select(sql3); 
            }
             else if(request.getParameter("bank").contains("SBT"))
            {
               String sql4= "select * from tbl_sbt";
            db.select(sql4); 
            }
  %>
  <table border="1" cellpadding="5">
            <caption><h2>Topics</h2></caption>
            <tr>
               
                <th>KeyWord</th>
                </tr>
<%   while(db.rs.next())
                           {
                          
%>
                  <tr><td><%=db.rs.getString("details")%></td></tr>             
    <%    }
}            
} catch(Exception e)
                {
                out.println(e);
                }

    
        
   
%>

               
               
                    
                  
     
     <%
         //String str=db.rs.getString("details");
         //FreeTTS f=new FreeTTS(str);
         //f.getDetails(str);
     
     %>

     <br> <br><br><br><br><br><br><br><br><br><br><br><br>

                    <footer id="footer">
                    <div class="col-md-8">
                        <p class="copyright">Copyright: <span>2015</span> . Design and Developed by <a href="http://www.Themefisher.com">Themefisher</a></p>
                    </div>
                    <div class="col-md-4">
                        <!-- Social Media -->
                        <ul class="social">
                            <li>
                                <a href="http://wwww.fb.com/themefisher" class="Facebook">
                                    <i class="ion-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://wwww.twitter.com/themefisher" class="Twitter">
                                    <i class="ion-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="Linkedin">
                                    <i class="ion-social-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://wwww.fb.com/themefisher" class="Google Plus">
                                    <i class="ion-social-googleplus"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer> <!-- /#footer -->
                
        </body>
    </html>
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                