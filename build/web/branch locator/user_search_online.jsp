<%-- 
    Document   : index
    Created on : Jan 25, 2016, 12:45:58 PM
    Author     : Jojo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="dbclass.dbconne"%>
<%-- 
    Document   : main_home
    Created on : Jan 18, 2016, 9:32:44 AM
    Author     : Jojo
--%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WELCOME | HOME</title>
<link type="text/css" rel="stylesheet" href="css/styles.css" />
<div id="header"><font color="#FFFFFF" size="32"><center>BEST KEYWORD COVER SEARCH</center></font></div>
<style>
      
      #map {
        height: 95%;
		background-color:#666666;
      }
    </style>
<script>
/*
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    var x=position.coords.latitude;	
    document.getElementById("lat").value = x;
    var x=position.coords.longitude;	
    document.getElementById("log").value = x;
}*/
</script>

	    <script>
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var map;
var infowindow;

function initMap() {
  var latlngStr = document.getElementById('lat').value;
  var latlngStr1 = document.getElementById('log').value;
  
  
  var pyrmont = {lat: parseFloat(latlngStr), lng: parseFloat(latlngStr1)};


 
  map = new google.maps.Map(document.getElementById('map'), {
    center: pyrmont,
    zoom: 15
  });


  infowindow = new google.maps.InfoWindow();

  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch({
    location: pyrmont,
    radius: 1000,
    types: ['<%=request.getParameter("search")%>']
  }, callback);
}

function callback(results, status) {
  if (status === google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
	 // alert(results[i].name);
    }
  }
}

function createMarker(place) {
  var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location
  });

  google.maps.event.addListener(marker, 'click', function() {
      alert(place.name+"        "+place.geometry.location.lat()+"   "+place.geometry.location.lng());
    infowindow.setContent(place.name);
    infowindow.open(map, this);
  });
}

    </script>
 <script language="javascript" type="text/javascript">
// Roshan's Ajax dropdown code with php
// This notice must stay intact for legal use
// Copyright reserved to Roshan Bhattarai - nepaliboy007@yahoo.com
// If you have any problem contact me at http://roshanbh.com.np
function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	

	function getCity(city) {		
		var strURL="findcity.jsp?city="+city;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('citydiv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
				
	}
</script>   
 

	
</head>
<body onLoad="getLocation()">
	
        <br /><br /><br /><br />
        <div id="container">
        	<div class="contents">
            	<center>
                    <form method="GET" action="#">
                      <table>
                    	<tr>
                        	<td>Search Query </td>
                            <td> : </td>
                            <%
                                 
                                %>
                            <td><input name="search" type="text" id="search" required="true"  list="keyword" />
                                Place :   <input type="text" name="place" id="place" required="true" onChange="getCity(this.value)" list="place" />
                             <!--<div id="citydiv">
                                <input type="hidden" name="lat" id="lat" value=""/>
                                <input type="hidden" name="log" id="log" value=""/>
                              </div>     -->
                                <datalist id="place">
                               <%
                                    
                                try
                                {
                               
                                String s="select * from statelist";
                              dbconne db= new dbconne();
                                db.select(s);
                                %>
                                <div id="citydiv">
                                <input type="hidden" name="lat" id="lat" value=""/>
                                <input type="hidden" name="log" id="log" value=""/>
                                </div>
                                 <%
                               while(db.rs.next())
                                {
                                    %>
                                    <option value="<%=db.rs.getString(2)%>">
                                    </option> 
                                  <%          
                                }}
                                catch(Exception e)
                               {
                               System.out.println(e);
                               }
                                %>
                                </datalist>
                                <datalist id="keyword">
                                    <option value="accounting">
<option value="airport">
<option value="amusement_park">
<option value="aquarium">
<option value="art_gallery">
<option value="atm">
<option value="bakery">
<option value="bank">
<option value="bar">
<option value="beauty_salon">
<option value="bicycle_store">
<option value="book_store">
<option value="bowling_alley">
<option value="bus_station">
<option value="cafe">
<option value="campground">
<option value="car_dealer">
<option value="car_rental">
<option value="car_repair">
<option value="car_wash">
<option value="casino">
<option value="cemetery">
<option value="church">
<option value="city_hall">
<option value="clothing_store">
<option value="convenience_store">
<option value="courthouse">
<option value="dentist">
<option value="department_store">
<option value="doctor">
<option value="electrician">
<option value="electronics_store">
<option value="embassy">
<option value="establishment">
<option value="finance">
<option value="fire_station">
<option value="florist">
<option value="food">
<option value="funeral_home">
<option value="furniture_store">
<option value="gas_station">
<option value="general_contractor">
<option value="grocery_or_supermarket">
<option value="gym">
<option value="hair_care">
<option value="hardware_store">
<option value="health">
<option value="hindu_temple">
<option value="home_goods_store">
<option value="hospital">
<option value="insurance_agency">
<option value="jewelry_store">
<option value="laundry">
<option value="lawyer">
<option value="library">
<option value="liquor_store">
<option value="local_government_office">
<option value="locksmith">
<option value="lodging">
<option value="meal_delivery">
<option value="meal_takeaway">
<option value="mosque">
<option value="movie_rental">
<option value="movie_theater">
<option value="moving_company">
<option value="museum">
<option value="night_club">
<option value="painter">
<option value="park">
<option value="parking">
<option value="pet_store">
<option value="pharmacy">
<option value="physiotherapist">
<option value="place_of_worship">
<option value="plumber">
<option value="police">
<option value="post_office">
<option value="real_estate_agency">
<option value="restaurant">
<option value="roofing_contractor">
<option value="rv_park">
<option value="school">
<option value="shoe_store">
<option value="shopping_mall">
<option value="spa">
<option value="stadium">
<option value="storage">
<option value="store">
<option value="subway_station">
<option value="synagogue">
<option value="taxi_stand">
<option value="train_station">
<option value="ravel_agency">
<option value="university">
<option value="veterinary_care">
<option value="zoo">
                     </datalist>
                            </td>
                        </tr>
                        <tr>
                        	<td></td>
                            <td></td>
                            <td><input type="submit" name="Submit" value="Submit" /></td>
                        </tr>
                    </table>
                        
                        <%
                            
                        if(request.getParameter("Submit")!=null)
                        {
                            
                             String a=request.getParameter("lat");
                             String b=request.getParameter("lng");
                            %>
                            
                                
                                
                                   <input type="hidden" name="lat" id="lat" value="<%=a%>"/>
                                  <input type="hidden" name="lng" id="lng" value="<%=b%>"/>   
                                    
                                
                             <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVXwgSfvHXXP-VibYL4XgyeMzagyJnEdk&signed_in=true&libraries=places&callback=initMap" async defer></script>
                            <%
                        }
                        else
                        {
                            %>
                            <script>
                                alert("No Data to Display");
                            </script>
                            <%
                        }
                        
                        %>
                    </form>
                    <div id="map">
					
					
					</div>
                    
                    
                 
            	</center>
            </div>
        </div>
</body>
</html>