<%-- 
    Document   : index
    Created on : 8 Feb, 2017, 6:08:33 AM
    Author     : gokul
--%><%@page import="dbclass.dbconne"%>


<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Banking Bot</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="css/ionicons.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="css/slider.css">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <!-- template main css file -->
        <link rel="stylesheet" href="css/main.css">
        <!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css">
        
        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- owl carouserl js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- bootstrap js -->

        <script src="js/bootstrap.min.js"></script>
        <!-- wow js -->
        <script src="js/wow.min.js"></script>
        <!-- slider js -->
        <script src="js/slider.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <!-- template main js -->
        <script src="js/main.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <!--
        ==================================================
        Header Section Start
        ================================================== -->
        <header id="top-bar" class="navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->
                    
                    <!-- logo -->
                    <div class="navbar-brand">
                        <a href="index.html" >
                            <img src="images/bba.png" alt="">
                        </a>
                    </div>
                    <!-- /logo -->
                </div>
                <!-- main menu -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="index.html" >Home</a>
                            </li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="talks.jsp">Service</a></li>
                           
                        </ul>
                    </div>
                </nav>
                <!-- /main nav -->
            </div>
        </header>
   
	
        <br><br><br><br><br>
        <div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">

      
      
      <div class="input-group">
        <h3>Search Different Banks States in India</h3>
        <form action ="#" method ="GET">
	<input type="text" id="state" name="state"/>
        
        <input type ="submit" id="search" value ="Search"name ="search">
          </form>
        
        <%
    if(request.getParameter("search")!= null)
    {
     String states=request.getParameter("state");
 
     dbconne db=new dbconne();
    
   String sql1 = "SELECT * FROM statelist ";
   db.select(sql1);
   %>
   <h2 style="color: limegreen;">Major Cities in <%=states%></h2>
   
   <%
       while(db.rs.next()){
       %>
       <label> <a href="cities.jsp?city=<%=db.rs.getString("city_id")%>"><%=db.rs.getString("city_name")%></a></label>
       <br>
     <%  
   }
    }

%>
<br><br><br>
    </center>
	<script>
		$("#state").autocomplete("getdata.jsp");
	</script>
	

        
        </button>
        </span>
      </div>
    </div>
<br><br>   <br><br><br><br><br>      


<div class="container">
  <h2></h2>       
  <img src="images/images1.jpeg" class="img-circle" alt="Cinque Terre" width="304" height="236"> 
  <img src="images/images2.png" class="img-circle" alt="Cinque Terre" width="304" height="236"> 
  <br><br><img src="images/images3.jpg" class="img-circle" alt="Cinque Terre" width="304" height="236"> 
  <img src="images/images4.jpeg" class="img-circle" alt="Cinque Terre" width="304" height="236"> 
  <br><br><img src="images/images5.jpg" class="img-circle" alt="Cinque Terre" width="304" height="236"> 
    <img src="images/images6.jpeg" class="img-circle" alt="Cinque Terre" width="304" height="236"> 

</div>
      
  

    <footer id="footer">
                <div class="container">
                    <div class="col-md-8">
                        <p class="copyright">Copyright: <span>2015-2017</span> Desgined and Developed by MaskGo Co-operates </a></p>
                    </div>
                    <div class="col-md-4">
                        <!-- Social Media -->
                        <ul class="social">
                            <li>
                                <a href="http://wwww.fb.com/themefisher" class="Facebook">
                                    <i class="ion-social-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://wwww.twitter.com/themefisher" class="Twitter">
                                    <i class="ion-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="Linkedin">
                                    <i class="ion-social-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="http://wwww.fb.com/themefisher" class="Google Plus">
                                    <i class="ion-social-googleplus"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer> <!-- /#footer -->
                
        </body>
    </html>
        